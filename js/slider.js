jQuery.easing.def = "easeOutBounce";
function slide( id ){
	return ".slide-"+id+" .slide-image";
}
function info( id ){
	return ".slide-"+id+" .slide-name, .slide-"+id+" .slide-desc, .slide-"+id+" .slide-btn";
}
function wrap( num ){
	if (num>0){
		return Math.abs(num%id);
	}else{
		return Math.abs((num+(id*5))%id);
	}
}
function onLeft( obj ){
	obj.css({
		"left":"-500px"
	});
	setTimeout(function(){obj.animate({
		"left":"-265px"
	},1000)},1000);
}
function onRight( obj ){
	obj.css({
		"right":"-1000px"
	});
	setTimeout(function(){obj.animate({
		"right":"-735px"
	},1000)},1000);
}
function onMiddle( obj ){
	obj.css({
		"top":"-500px",
		"left":"235px"
	});
	setTimeout(function(){obj.animate({
		"top":"0px"
	},1000)},1000);
}

function goLeft(){
	clearInterval(intId);
	$(slide(farLeft)).animate({
		"left": "235px"
	},{duration:150, easing: "easeOutSine"});
	$(slide(wrap(farLeft-1))).css("left","-500px");
	$(slide(wrap(farLeft-1))).animate({
		"left": "-265px"
	},{duration:150, easing: "easeOutSine"});
	$(slide(wrap(farLeft+1))).animate({
		"left": "735px"
	},{duration:150, easing: "easeOutSine"});
	$(slide(wrap(farLeft+2))).animate({
		"left": "970px"
	},{duration:150, easing: "easeOutSine"});
	howFar = -265;
	farLeft = wrap(farLeft-1);
	$( info( wrap(farLeft+2) ) ).stop().fadeOut({duration:500, easing: "linear"});
	$( info( wrap(farLeft+1) ) ).stop().fadeIn({duration:500, easing: "linear"});
	sliderState = false;
	$(".slider .btn-pause img").attr("src","img/play.png");
}
function goRight(){
	clearInterval(intId);
	$(slide(farLeft)).animate({
		"left": "-500px"
	},{duration:150, easing: "easeOutSine"},function(){
		$(slide(farLeft)).css("left","970px");
	});
	$(slide(wrap(farLeft+1))).animate({
		"left": "-265px"
	},{duration:150, easing: "easeOutSine"});
	$(slide(wrap(farLeft+2))).animate({
		"left": "235px"
	},{duration:150, easing: "easeOutSine"});
	$(slide(wrap(farLeft+3))).css("left","970px");
	$(slide(wrap(farLeft+3))).animate({
		"left": "735px"
	},{duration:150, easing: "easeOutSine"});
	howFar = -265;
	farLeft = wrap(farLeft+1);
	$( info( wrap(farLeft) ) ).stop().fadeOut({duration:500, easing: "linear"});
	$( info( wrap(farLeft+1) ) ).stop().fadeIn({duration:500, easing: "linear"});
	sliderState = false;
	$(".slider .btn-pause img").attr("src","img/play.png");
}

var intId;
var sliderState;
function animate() {
	intId = setInterval(function(){
		howFar -= 1;
		if ( howFar<=(-500) ){
			$( slide(farLeft) ).css("left","970px");
			farLeft = wrap(farLeft+1);
			howFar = 0;

			$( info( farLeft ) ).fadeOut({duration:500, easing: "linear"});
			$( info( wrap(farLeft+1) ) ).fadeIn({duration:500, easing: "linear"});
		}
		$( slide(farLeft) ).css("left", (howFar) );
		$( slide( wrap(farLeft+1) ) ).css("left", (howFar+500) );
		$( slide( wrap(farLeft+2) ) ).css("left", (howFar+1000) );
	},15);
	sliderState = true;
}
function pause() {
	clearInterval(intId);
	$(slide(farLeft)).animate({
		"left": "-265px"
	});
	$(slide(wrap(farLeft+1))).animate({
		"left": "235px"
	});
	$(slide(wrap(farLeft+2))).animate({
		"left": "735px"
	});
	howFar = -265;
	$(".slider .btn-pause img").attr("src","img/play.png");
	sliderState = false;
}

$(".slider").append("<div class='btn-pause'><img src='img/pause.png' alt='pause slideshow' /></div><div class='btn-left'><img src='img/left.png' alt='last slide' /></div><div class='btn-right'><img src='img/right.png' alt='next slide' /></div>");
$(".slider .btn-pause").css({
	"position": "relative",
	"top": "20px",
	"left": "20px",
	"z-index": "9999"
})

$(".slider .btn-left").css({
	"position": "absolute",
	"top": "240px",
	"z-index": "9999",
	"display": "inline-block"
})

$(".slider .btn-right").css({
	"position": "absolute",
	"top": "240px",
	"left": "876px",
	"z-index": "9999",
	"display": "inline-block"
})

$(".slider .btn-pause").click(function(){
	if(sliderState){
		pause();
	}else{
		animate();
		$(".slider .btn-pause img").attr("src","img/pause.png");
	}
})

$(".slider .btn-left").click(function(){
	if(sliderState){
		goLeft()
	}else{
		goLeft();
	}
})

$(".slider .btn-right").click(function(){
	if(sliderState){
		goRight()
	}else{
		goRight();
	}
})

$(".slide[title]").each(function(){
	$(this).append("<div class='slide-btn'><a href='"+$(this).attr("title")+"' target='_blank'><img src='img/btn-homepage.png' alt='HOMEPAGE' /></a></div>");
});

id = 0;
$(".slider .slide").each(function(){
	$(this).addClass( "slide-"+id );
	$( info( id ) ).hide();
	if (id > 2){
		$( slide(id) ).css("left","970px");
	}
	id++;
});
onLeft( $( slide(0) ) );
onMiddle( $( slide(1) ) );
onRight( $( slide(2) ) );

farLeft = 0;
howFar = -265;

$( info( wrap(farLeft+1) ) ).fadeIn(1000);

setTimeout(function() {
	animate();
},2500);