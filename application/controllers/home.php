<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index()
	{
		$data = array(
			'title' => 'Home Page'
		);
		$this->load->view("includes/header.php", $data);
		$this->load->view('home');
		$this->load->view("includes/footer.php");
	}
}
