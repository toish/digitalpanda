<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Portfolio extends CI_Controller {
	public function index()
	{
		$data = array(
			'title' => 'Portfolio'
		);
		$this->load->view("includes/header.php", $data);
		$this->load->view('portfolio');
		$this->load->view("includes/footer.php");
	}
}
