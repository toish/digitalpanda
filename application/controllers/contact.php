<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {
	public function index()
	{
		$data = array(
			'title' => 'Contact Me'
		);
		$this->load->view("includes/header.php", $data);
		$this->load->view('contact', $data);
		$this->load->view("includes/footer.php");
	}
	public function sendemail(){
		$form_data = $this->input->post();
    	$name = $this->input->post("name");
    	$email = $this->input->post("email");
    	$subject = $this->input->post("subject");
    	$message = $this->input->post("message");
	    if ($email=="") {
	    	$data = array(
	    		'title' => 'Error! ;n;',
	    		'error' => 'You forgot your email!',
	    		'forceback' => 'oui'
	    	);
			$this->load->view("includes/header.php", $data);
			$this->load->view('contacterror',$data);
			$this->load->view("includes/footer.php");
    	}elseif ($name=="") {
	    	$data = array(
	    		'title' => 'Error! ;n;',
	    		'error' => 'You forgot your name!',
	    		'forceback' => 'you bet\'cha ass'
	    	);
			$this->load->view("includes/header.php", $data);
			$this->load->view('contacterror',$data);
			$this->load->view("includes/footer.php");
    	}elseif ($subject=="") {
	    	$data = array(
	    		'title' => 'Error! ;n;',
	    		'error' => 'You forgot your subject!',
	    		'forceback' => 'da'
	    	);
			$this->load->view("includes/header.php", $data);
			$this->load->view('contacterror',$data);
			$this->load->view("includes/footer.php");
    	}elseif ($message=="") {
	    	$data = array(
	    		'title' => 'Error! ;n;',
	    		'error' => 'You forgot your message!',
	    		'forceback' => 'cause\' yolo right?'
	    	);
			$this->load->view("includes/header.php", $data);
			$this->load->view('contacterror',$data);
			$this->load->view("includes/footer.php");
    	}else{
			$this->load->helper('security');
			$this->load->library('email');
			$this->email->to('techpanda@digitalpanda.ca');
			$this->email->from(($this->security->xss_clean($email)));
			$this->email->subject('DIGITALPANDA MAIL : ' . ($this->security->xss_clean($subject)));
			$this->email->message(($this->security->xss_clean($name)) . " said:\n\n" . ($this->security->xss_clean($message)));

			$this->email->send();

			$data = array(
				'title' => 'Thanks!',
				'name' => $name,
				'forceback' => 'yep'
			);

			$this->load->view("includes/header.php", $data);
			$this->load->view('contactthanks',$data);
			$this->load->view("includes/footer.php");
		}
	}
}
