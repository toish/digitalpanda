<section class="nopadding">
	<div class="slider">
		<div class="slide" title="http://crombz.com">
			<div class="slide-image"><img src="img/slider/crombz.png" alt="Crombz Logo"></div>
			<div class="slide-name">Crombz</div>
			<div class="slide-desc">Crombz is a web application that allows anyone to make the world their blog! Using the power of the modern phone's GPS, you can leave notes, thoughts, and ideas, anywhere in the world! I approched Patrick Belleux over twitter with a few design ideas and the rest is history.</div>
		</div>
		<div class="slide" title="http://amicipizzaandpastabar.com/">
			<div class="slide-image"><img src="img/slider/amici.png" alt="Amici Website"></div>
			<div class="slide-name">Amici</div>
			<div class="slide-desc">Amici is one of those little hidden gems, that once you find, you know you’ve found your new favorite place to dine out and socialize in Whitby. Working as part of the talented team at Citrus Media, I helped to code the Masterprint website using HTML, CSS, JS and the Code Igniter framework.</div>
		</div>
		<div class="slide">
			<div class="slide-image"><img src="img/slider/streamwalk.png" alt="StreamWalk Tech Logo"></div>
			<div class="slide-name">StreamWalk Technology</div>
			<div class="slide-desc">StreamWalk Technology is a company based solely on the purpose of making the world a better place though technology and imagination. I have the great priviliage to be a part of the StreamWalk team in the creation of the social platform, Aardwolf.</div>
		</div>
		<div class="slide" title="http://digitalpanda.ca">
			<div class="slide-image"><img src="img/slider/digitalpanda.png" alt="Digital Panda Logo"></div>
			<div class="slide-name">Digital Panda</div>
			<div class="slide-desc">I created the Digital Panda brand for a communications technology project in 2014, over about one sleepless night. The inspiration for the red panda in the logo came from a random google search for "Cool Animals". Red pandas are pretty cool.</div>
		</div>
		<div class="slide" title="https://soundcloud.com/nicholas-jack-rabbit-cuave">
			<div class="slide-image"><img src="img/slider/journeycuave.png" alt="Journey Cuave Logo, and album covers"></div>
			<div class="slide-name">Journey Cuave</div>
			<div class="slide-desc">Journey Cuave is an aspiring electro-trance music artist, that needed a few album covers and a logo. I went with a layed-back feel with most of the work, sticking to the style of his music.</div>
		</div>
		<div class="slide">
			<div class="slide-image"><img src="img/slider/tigercowz.png" alt="TigerCowZ Logo"></div>
			<div class="slide-name">TigerCowZ Entertainment</div>
			<div class="slide-desc">TigerCowZ Entertainment is a media company that makes gameplay videos and various sketchs. Though we had a rough patch while we worked though some of our other projects, such as "GrahamCraft", we plan to continue to make high-quality content for internet audiances.</div>
		</div>
		<div class="slide" title="http://masterprint.ca">
			<div class="slide-image"><img src="img/slider/masterprint.png" alt="Masterprint Website"></div>
			<div class="slide-name">Masterprint</div>
			<div class="slide-desc">Masterprint is a commercial printer serving the Durham region. They work hard to create the highest quality prints and the lowest cost. Working as part of the talented team at Citrus Media, I helped to code the Masterprint website using HTML, CSS, JS and the Code Igniter framework.</div>
		</div>
		<div class="slide" title="http://toscabanquethall.com">
			<div class="slide-image"><img src="img/slider/tosca.png" alt="Tosca Banquet Hall Website"></div>
			<div class="slide-name">Tosca Banquet Hall</div>
			<div class="slide-desc">Tosca Banquet Hall truly prides themselves on providing each and every one of their clients with the highest standard of service from the initial planning through to the day of the event. Whether that event is a formal business meeting, or you and your significant others' big day. Working as part of the talented team at Citrus Media, I helped to code the Masterprint website using HTML, CSS, JS and the Code Igniter framework.</div>
		</div>
	</div>
</section>

<script src="js/slider.js"></script>