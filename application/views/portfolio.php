<section class="nopadding">
	
	<div id="projects" class="js-masonry" data-masonry-options='{ "columnWidth": 242.5, "itemSelector": ".project" }'>
		<div class="project h1 w1" data-image="oneill.png"></div>
		<div class="project h1 w1" data-image="omarkorea.png"></div>
		<div class="project h2 w2" data-image="withglasses.gif"></div>
		<div class="project h1 w1" data-image="oja.gif"></div>
		<div class="project h1 w1" data-image="fr2tso.png"></div>
		<div class="project h2 w2" data-image="swi.png"></div>
		<div class="project h1 w1" data-image="amici.gif"></div>
		<div class="project h1 w1" data-image="withglasses.gif"></div>
		<div class="project h1 w2" data-image="gabe.gif"></div>
	</div>

</section>

<script type="text/javascript">
	$(document).ready(function(){
		$('#projects .project').each(function(){
			$(this).css('background-image','url(img/portfolio/'+($(this).attr('data-image'))+')');
		})
	});
</script>