<section class="nopadding red">
	<div class="heading"><?php echo $title ?></div>
	<div class="subheading">
		Feel free to contact me with any and all questions, comments or work! I'm happy to talk with anyone, even just say "Hi" if ya want to!
	</div>

	<form class="contact-form" action="contact/sendemail" method="post">

		<div class="form-left">
			<label for="name">Name:</label><br>
			<input type="text" name="name" />
		</div>

		<div class="form-right">
			<label for="email">Email:</label><br>
			<input type="text" name="email" />
		</div><br>

		<label for="subject">Subject:</label><br>
		<input type="text" name="subject" /><br><br>

		<label for="message">Message:</label><br>
		<textarea name="message"></textarea><br><br>

		<input type="submit" value="Send" />

	</form>
</section>