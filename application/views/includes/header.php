<?php
	if (isset($forceback)) {
		$extension = "../";
	}else{
		$extension = "";
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $title." | Digital Panda"; ?></title>
		<meta charset="UTF-8" >
		<link rel="stylesheet" type="text/css" href="<?php echo $extension ?>fonts/aaargh/stylesheet.css">
		<link rel="stylesheet" type="text/css" href="<?php echo $extension ?>css/main.css">
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$.backstretch("<?php echo $extension ?>img/bg.jpg");
			});
		</script>
		<script type="text/javascript" src="<?php echo $extension ?>js/navbar.js"></script>
		<script type="text/javascript" src="<?php echo $extension ?>js/masonry.pkgd.min.js"></script>
		<link rel="shortcut icon" href="<?php echo $extension ?>/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo $extension ?>/favicon.ico" type="image/x-icon">

		<meta charset="UTF-8">

		<script> // Google Analytics
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-50105444-1', 'digitalpanda.ca');
			ga('require', 'displayfeatures');
			ga('send', 'pageview');
		</script>
	</head>
	<body>
		<header>
			<a href="<?php echo site_url(); ?>">
				<div class="logo">
					<img class="logo-panda" src="<?php echo $extension ?>img/logo-panda.png" alt="Digital Panda" />
					<img class="logo-wordmark" src="<?php echo $extension ?>img/logo-wordmark.png" alt="Digital Panda" />
				</div>
			</a>
			<nav>
				<img src="<?php echo $extension ?>img/header-bar.png" alt="" />
				<div class="buttons">
					<a href="http://digitalpanda.ca"><div class="nav-btn portfolio-btn"></div></a>
					<!--a href="about"><div class="nav-btn about-btn"></div></a-->
					<a href="contact"><div class="nav-btn contact-btn"></div></a>
				</div>
				<div class="buttons-social">
					<a href="https://twitter.com/some_red_panda" target="_blank"><div class="nav-btn-social twitter-btn"></div></a>
					<a href="https://www.facebook.com/gomy1020" target="_blank"><div class="nav-btn-social facebook-btn"></div></a>
				</div>
			</nav>
		</header>
		<div class="content">